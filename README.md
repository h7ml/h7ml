1. 千亿房企阳光城爆雷，正式宣布违约，哪些信息值得关注？ [:link:](https://www.zhihu.com/question/582286014)
2. 如何看待韩驻华使馆称「望韩中涉疫矛盾尽快化解」？ [:link:](https://www.zhihu.com/question/582581015)
3. 开年电影《流浪地球 2》和剧版《三体》都取得封神级成功，为什么动画版《三体》失败了？ [:link:](https://www.zhihu.com/question/582221408)
4. 是什么原因造成日本成为首个从高度发达国家跌落成中等发达国家的？ [:link:](https://www.zhihu.com/question/582143847)
5. Chat GPT有多高的技术壁垒？国内外除了OpenAI还有谁可以做到类似程度？ [:link:](https://www.zhihu.com/question/581806122)
6. 《狂飙》里的四个女人陈书婷，孟钰，高启兰和黄瑶，哪个更惨？明明拥有了金钱豪宅豪车的她们结局仍旧不好？ [:link:](https://www.zhihu.com/question/582021418)
7. 姚明 12 岁女儿身高直逼 1 米 9，你在哪一刻感受到过基因的强大？ [:link:](https://www.zhihu.com/question/577497208)
8. 华为王军停职，余承东独揽大权，这将给该企业带来哪些变化？ [:link:](https://www.zhihu.com/question/582389002)
9. 在三体世界中三日凌空之后，包括三体人在内的物种几乎灭绝，那后续的三体人知识和智慧该如何传承？ [:link:](https://www.zhihu.com/question/582576867)
10. 伊朗驻华大使馆辟谣「情侣公开场合跳舞被判 10 年」，被判刑是因煽动骚乱、危害国家安全，如何看待此事？ [:link:](https://www.zhihu.com/question/581798809)
<details>
<summary>11 ~ 20</summary>

11. 国产千元机都支持120Hz高刷，为什么售价5000多的iPhone 14却没有呢？ [:link:](https://www.zhihu.com/question/577526220)
12. 《武林外传》中李大嘴的厨艺算什么水平？ [:link:](https://www.zhihu.com/question/37264128)
13. 四川夫妇因经济困难无力抚养孩子，便将女儿送养，后悔后寻女 30 年终于认亲团圆，如何看待此事？ [:link:](https://www.zhihu.com/question/582414685)
14. 近七成俄企用中国制造替代西方设备，这一调查结果说明什么？ [:link:](https://www.zhihu.com/question/582313229)
15. 储殷回应「35 岁后做废物很舒服言论」，称「不是什么专家意见，是有感而发」，如何看待这一回应？ [:link:](https://www.zhihu.com/question/582606872)
16. 一研究者提前 3 天靠太阳系天体间的位置关系准确预言土耳其地震区域和震级，真有可能精准预测地震吗？ [:link:](https://www.zhihu.com/question/582742809)
17. 任天堂 Switch 销量已达 1.21 亿台，这一数据说明了什么？ [:link:](https://www.zhihu.com/question/582298645)
18. 《狂飙》剧情删改了哪些地方？原版的剧情是什么？ [:link:](https://www.zhihu.com/question/581387496)
19. 怎么评价《霍格沃茨之遗》这款游戏，值得入手吗？ [:link:](https://www.zhihu.com/question/582554515)
20. 哈尔滨一无人机不听劝阻干扰企鹅，景区出动无人机将其撞毁，无人机飞行应当遵循哪些法律法规？ [:link:](https://www.zhihu.com/question/577873319)
</details>
<details>
<summary>21 ~ 30</summary>

21. 电影《流浪地球 2》中既然世界各国同意交出核弹炸月亮，为什么密码还需要计算？ [:link:](https://www.zhihu.com/question/580829044)
22. 购房者「提前还房贷」受阻，银行为何要阻止提前还贷？什么群体适合提前还？ [:link:](https://www.zhihu.com/question/582439081)
23. 为什么《生活大爆炸》里看上去最不靠谱的霍华德和伯纳黛特的生活却最和睦？ [:link:](https://www.zhihu.com/question/41139743)
24. 有哪些景点，曾让你感慨「票价真值」？ [:link:](https://www.zhihu.com/question/582499972)
25. 大家有哪些让皮肤变好的「好习惯」可以分享下吗？ [:link:](https://www.zhihu.com/question/581141136)
26. 以后考公竞争会越来越大吗？ [:link:](https://www.zhihu.com/question/582282749)
27. 现在很多孩子从小就接受普通话教育，是否还需要学习家乡方言呢？ [:link:](https://www.zhihu.com/question/576333921)
28. 领导的口头禅是「这么简单的事情都做不好」，员工应该如何应对？ [:link:](https://www.zhihu.com/question/582376146)
29. 男子放鞭炮导致邻居男童手臂截肢，该付什么法律责任?相关部门应该如何监管大威力炮仗？ [:link:](https://www.zhihu.com/question/582400025)
30. 土耳其地震已致该国 5894 人死亡 34810 人受伤，目前救援情况如何？后续救援有哪些难点？ [:link:](https://www.zhihu.com/question/582739525)
</details>
<details>
<summary>31 ~ 40</summary>

31. 每次都告诉自己要好好说话，可是总是控制不住吼孩子，怎么办？ [:link:](https://www.zhihu.com/question/581212744)
32. 情人节有没有必要给相亲对象送礼物，送的话怎么选购礼物才能「既走心又有排面」？ [:link:](https://www.zhihu.com/question/582648062)
33. 为什么《狂飙》里孟德海都搞不定李有田？ [:link:](https://www.zhihu.com/question/580295479)
34. 恋爱里「彼此同频」有多重要？ [:link:](https://www.zhihu.com/question/581984335)
35. 韩国组合 BIGBANG 前成员胜利将于 11 日出狱，这将会对他的职业生涯产生多大影响？ [:link:](https://www.zhihu.com/question/582418284)
36. 杭州两套拆迁房让一对亲姐妹闹掰，如何看待此事？遗产继承需要注意哪些法律问题？ [:link:](https://www.zhihu.com/question/582593170)
37. 近日土耳其地震，有网友透露《非正式会谈》嘉宾唐小强被埋废墟下，目前情况如何？有哪些信息值得关注？ [:link:](https://www.zhihu.com/question/582599103)
38. 如何看待生命中的离别？ [:link:](https://www.zhihu.com/question/580795404)
39. 如何在前途和现实中做选择? [:link:](https://www.zhihu.com/question/582671819)
40. 多家期刊、出版机构禁止将 ChatGPT 列为论文合著者，该类人工智能的普及将对人类产生哪些影响？ [:link:](https://www.zhihu.com/question/582618047)
</details>
<details>
<summary>41 ~ 50</summary>

41. 如果读书是为了找工作，那工作又是为了什么？ [:link:](https://www.zhihu.com/question/582686405)
42. 公司局域网的IP地址池不够用怎么办？ [:link:](https://www.zhihu.com/question/581670921)
43. 有了孩子后，你们还会过情人节吗？都是如何度过的？ [:link:](https://www.zhihu.com/question/581564689)
44. 打工人一到下午就困得睁不开眼，有什么快速提神的办法？ [:link:](https://www.zhihu.com/question/557377673)
45. 文化人如何隐晦地表达「我爱你」？ [:link:](https://www.zhihu.com/question/581561282)
46. 有哪些便宜量大，适合在宿舍里分着和舍友一起吃的零食？ [:link:](https://www.zhihu.com/question/581916353)
47. 有哪些「句句不提爱，但字字都是爱」的情人节表白文案？ [:link:](https://www.zhihu.com/question/581559060)
48. 情人节想送女友一块手表，预算 3000-5000 内，有哪些合适的品牌和款式推荐？ [:link:](https://www.zhihu.com/question/582087812)
49. 有什么食物适合节后给肠胃「刮刮油」？ [:link:](https://www.zhihu.com/question/580909922)
50. 有哪些适合情人节的高级文案？ [:link:](https://www.zhihu.com/question/581555007)
</details><details>
<summary>bilibili</summary>

1. 【年度巨献】原神同人大电影 「暗潮」 [:link:](//www.bilibili.com/video/BV1hT411d7Fd)
2. 公开呼吁取关？！一条视频席卷全国，衣戈猜想走红真的是偶然吗？ [:link:](//www.bilibili.com/video/BV1WD4y1N7jJ)
3. 21世纪如何复兴德意志第二帝国？【神奇组织04】 [:link:](//www.bilibili.com/video/BV12Y411q7S1)
4. 探秘全世界最大的枪店！是什么体验？40年经典老店！库存上万支枪！ [:link:](//www.bilibili.com/video/BV1w8411G7LW)
5. 我，药系天王 [:link:](//www.bilibili.com/video/BV1FA411k7Vk)
6. 评分6.1！彻底坠毁！德凯奥特曼完结吐槽 [:link:](//www.bilibili.com/video/BV1CR4y1z7Ae)
7. 此乃坤拳，乾坤镜方可修成，我以致乾坤镜九星巅峰，颤抖吧ikun们 [:link:](//www.bilibili.com/video/BV1Wx4y1j7tA)
8. 两分钟视频大哥两次落泪，相信大哥一定可以东山再起！ [:link:](//www.bilibili.com/video/BV1X84y157bq)
9. 大雄...已经...无所谓了...【怀旧篇】！！！ [:link:](//www.bilibili.com/video/BV1pe4y1N72K)
10. 无厘头西游《车迟国斗法》中 [:link:](//www.bilibili.com/video/BV1nj411M7ns)
<details>
<summary>11 ~ 20</summary>

11. 学姐让你晚上来，你就得来 [:link:](//www.bilibili.com/video/BV1NG4y1T7Mu)
12. BSS (SEVENTEEN) 'Fighting (Feat. Lee Young Ji)' Official MV [:link:](//www.bilibili.com/video/BV1yx4y157Sc)
13. 制作流浪猫过冬三层猫窝（改造） [:link:](//www.bilibili.com/video/BV12x4y1j7ot)
14. 《原神》EP - 拈骰冥思之夜 [:link:](//www.bilibili.com/video/BV1KG4y1T76A)
15. 妈妈一进入我的房间，各种东西就会开始失踪了 [:link:](//www.bilibili.com/video/BV1K8411u7Yv)
16. 我将数十张试卷拼接成了一张试卷 [:link:](//www.bilibili.com/video/BV1wR4y1z7Xk)
17. “于西方拍卖会上流拍的东方戏曲人偶。”……我重回故里，而她们客死异乡。 [:link:](//www.bilibili.com/video/BV17A411679Z)
18. 他的未来规划，真的有我！ [:link:](//www.bilibili.com/video/BV15j411M7ik)
19. 《明日方舟》全新故事「春分」活动宣传PV [:link:](//www.bilibili.com/video/BV1aA4116773)
20. 当退坑2年的老玩家打开最新版《我的世界》 [:link:](//www.bilibili.com/video/BV1sD4y1T75A)
</details>
<details>
<summary>21 ~ 30</summary>

21. 课 堂 请 勿 对 对 子【大肠篇】！！！ [:link:](//www.bilibili.com/video/BV1Fx4y177Lo)
22. 卿月花灯彻夜明，吟肩随处倚倾城。复原古代会“走”的灯笼：走马灯 [:link:](//www.bilibili.com/video/BV1R8411M7MM)
23. 你看过这些动画片吗？这是一个完整的童年！ [:link:](//www.bilibili.com/video/BV1TD4y1N7Ni)
24. 【STN快报第七季03】科乐美直言将会剽窃玩家创意 [:link:](//www.bilibili.com/video/BV1Rd4y1n7rE)
25. 江西小伙骑行黑龙江，路边铁皮房扎营煮火锅吃，晚上零下26度比冰柜还冷 [:link:](//www.bilibili.com/video/BV1HA41167RB)
26. 当妈妈体验我极度懒惰的一天，她疯了… [:link:](//www.bilibili.com/video/BV19d4y1n7V2)
27. 蛋黄派尿酸太低了，带他去海鲜市场，买几只大螃蟹补补身体 [:link:](//www.bilibili.com/video/BV1EY411D7Mg)
28. 玩原神救了我一命，甚至有了孩子。 [:link:](//www.bilibili.com/video/BV18e4y1A7eU)
29. 这就是外卖小哥心动时刻？ [:link:](//www.bilibili.com/video/BV1jd4y1H7g3)
30. 看完视频，零基础也能发一篇二区SCI [:link:](//www.bilibili.com/video/BV1bx4y1j7Sd)
</details>
<details>
<summary>31 ~ 40</summary>

31. 耗时半个月，我给表妹做了一张触摸感应桌 [:link:](//www.bilibili.com/video/BV1S8411M7bW)
32. 你女朋友狠起来能有多猛？？ [:link:](//www.bilibili.com/video/BV1Qx4y1j7Fu)
33. 搬新家啦！芬兰一家人体验铜锅涮羊肉全家狂喜！现切肥羊卷吃到撑！自制东北大拉皮绝了！温居派对狂欢爽翻天！ [:link:](//www.bilibili.com/video/BV1aG4y1U7VX)
34. 伤 敌 一 千，自 损 一 千 [:link:](//www.bilibili.com/video/BV1ke4y1A7BC)
35. 十分钟，完成一条灵动的尾巴 [:link:](//www.bilibili.com/video/BV1yx4y1j7LU)
36. 第一次玩原神，请问公子是这么打的吗 [:link:](//www.bilibili.com/video/BV1Wy4y1D7g8)
37. 海绵宝宝你怎么了！海绵宝宝：宇宙摇摆 [:link:](//www.bilibili.com/video/BV1Bv4y1t7nu)
38. 《 奇 怪 的 修 猫 修 狗 出 现 了 》 [:link:](//www.bilibili.com/video/BV1ud4y1n7sU)
39. 汤姆配音大赛第一名 [:link:](//www.bilibili.com/video/BV1984y157NZ)
40. 有这样的老婆，我能吹一辈子！ [:link:](//www.bilibili.com/video/BV1Ry4y1D7HV)
</details>
<details>
<summary>41 ~ 50</summary>

41. 【原神手书】♛来自四神的压迫力♛～王牌特工们的「间谍过家家」～ [:link:](//www.bilibili.com/video/BV1Tv4y1b73i)
42. 他反复问她有没有遗憾，更让人遗憾了 [:link:](//www.bilibili.com/video/BV11Y411q7KW)
43. 全网在夸的“自助餐天花板”，我被现场CPU了！有些餐厅啊，别太欺负小白了吧。 [:link:](//www.bilibili.com/video/BV1MD4y1N7Cy)
44. 玩电竞、拍段子、老年课堂......这家年轻人办的养老院“火”了 [:link:](//www.bilibili.com/video/BV1Y8411g7dn)
45. 厨师长教你：用泡面做“鸡蛋肉丝炒面”，干香美味，方便简单 [:link:](//www.bilibili.com/video/BV1BT411X7FK)
46. 【TF家族】2023新年音乐会《瞬间》（制作篇全记录）中 [:link:](//www.bilibili.com/video/BV1eA41167N3)
47. 30岁硕士攒100w吃息退休实践，新年开门红200w不是梦 [:link:](//www.bilibili.com/video/BV1fT411X7PD)
48. 【318大乱斗】别哭啊战士！铁人三项再次升级⚡318大乱斗实况解说 [:link:](//www.bilibili.com/video/BV1Bs4y1W7j6)
49. 这次的结果总算有了起色 [:link:](//www.bilibili.com/video/BV1sv4y1b7XN)
50. 2小时20分，俯卧撑3200个！ [:link:](//www.bilibili.com/video/BV1vA411z7bk)
</details>
<details>
<summary>51 ~ 60</summary>

51. 【阿斗】口碑收视双爆表，2023开年王炸，投资超1亿美金！同名游戏改编《最后生还者》 [:link:](//www.bilibili.com/video/BV1ae4y1A714)
52. 速通玩家在NPC眼中的样子 [:link:](//www.bilibili.com/video/BV1cM411v79w)
53. 喵斯：“什么 B 动静 “ [:link:](//www.bilibili.com/video/BV1WD4y1N78B)
54. 【半佛】小天才过时了，老天才电话手表即将打穿市场。 [:link:](//www.bilibili.com/video/BV19x4y1j7vx)
55. 今天给全校孩子煮鸡蛋 早餐吃得好 才能长高高身体棒 看到班上好几个孩子自己不舍得吃还要留给弟弟妹妹吃的时候 满满的心疼与感动.. [:link:](//www.bilibili.com/video/BV12T411d71H)
56. 【九转大肠俞涛】B站我来了，鬼畜视频可以直接@我了！ [:link:](//www.bilibili.com/video/BV1bM411e7dJ)
57. 年兽：你管这叫烟花？？？ [:link:](//www.bilibili.com/video/BV19v4y1t7Cg)
58. 开学传奇，世界末日 [:link:](//www.bilibili.com/video/BV18x4y1j7Ew)
59. 狂飙诗词大赛你还知道哪些？ [:link:](//www.bilibili.com/video/BV1GA41167av)
60. 写不完了，开摆！ [:link:](//www.bilibili.com/video/BV17Y411D7xn)
</details>
<details>
<summary>61 ~ 70</summary>

61. 你3级这点血量敢1v2！？？不公平！重赛！重赛！ [:link:](//www.bilibili.com/video/BV1D24y1B7bn)
62. 【魔圆】“ 魔女的舞步 ” [:link:](//www.bilibili.com/video/BV1qx4y1j7B5)
63. 故 意 找 茬 [:link:](//www.bilibili.com/video/BV1vM411e7XP)
64. 满级高启强屠杀新手村 [:link:](//www.bilibili.com/video/BV1vd4y1n7c6)
65. 回国第一件事 狂炫中式早餐 29元直接吃撑 [:link:](//www.bilibili.com/video/BV1KG4y1S7XW)
66. 把烂梗玩成了王炸，把三农做成了事业，说过的承诺我做到了！ [:link:](//www.bilibili.com/video/BV17R4y1z7vF)
67. 韩国人热爱碳水的一生 在“碳水界”来看也很炸裂 [:link:](//www.bilibili.com/video/BV1aR4y1z7Ep)
68. “读孙子兵法,品启强人生” [:link:](//www.bilibili.com/video/BV1MG4y1S7cm)
69. “大肠…已经…无所谓了…” [:link:](//www.bilibili.com/video/BV19R4y1B72L)
70. 耗时7小时，揭秘酒店做的非常好吃的【九转大肠】入嘴瞬间值了！ [:link:](//www.bilibili.com/video/BV1qs4y1W7D5)
</details>
<details>
<summary>71 ~ 80</summary>

71. 一百万粉感谢！+我从小到大的环境展示（+画画的变化） [:link:](//www.bilibili.com/video/BV1my4y1X7tN)
72. 爆笑！火力全开吐槽《无名》和王一博演技！ [:link:](//www.bilibili.com/video/BV1MT411d7nc)
73. 看完这视频，你们会明白为啥市场上有这么多卖活禽活鱼的商家问你要不要宰杀了，因为死无对证！ [:link:](//www.bilibili.com/video/BV1jG4y1S7bj)
74. 完结撒花！挑战1W元通关植物大战僵尸2#14 [:link:](//www.bilibili.com/video/BV1ej411M7i7)
75. 不同类型的人表白被拒后的不同回复 [:link:](//www.bilibili.com/video/BV1hy4y1D734)
76. ☀⚡阳光开朗大男孩⚡☀ [:link:](//www.bilibili.com/video/BV1Nj411K74j)
77. 大肠我只吃带馅的 [:link:](//www.bilibili.com/video/BV1Cy4y1D7HK)
78. 啊？3.0 [:link:](//www.bilibili.com/video/BV1k24y1B73G)
79. 网络这块玩的就是真实！欢迎兄弟们来参观体验喝茶蹭饭，买不买东西真的无所谓 [:link:](//www.bilibili.com/video/BV1me4y1N7DG)
80. 【全球首通】韩国人做的自制谱 中国人也能第一时间拿下 [:link:](//www.bilibili.com/video/BV1184y1V7q8)
</details>
<details>
<summary>81 ~ 90</summary>

81. 司马懿：这洋妞开了，肯定开锁血了 [:link:](//www.bilibili.com/video/BV1yG4y1U7DG)
82. 双重诅咒 我的世界永恒的MC生存 二周目EP13 [:link:](//www.bilibili.com/video/BV1UG4y1U7XB)
83. 【原神】仇敌仿佛众水翻腾/须弥3.4主线⑨/预兆如窃贼到来/千壑沙地/原神3.4/须弥世界任务 [:link:](//www.bilibili.com/video/BV1kx4y157Gh)
84. 跟原味大师学的原味九转大肠真的很香！ [:link:](//www.bilibili.com/video/BV1Js4y1Y7mU)
85. 花了三天时间，终于给我家狗造了一个冰屋 [:link:](//www.bilibili.com/video/BV13x4y1j7Bz)
86. 名 侦 探 [:link:](//www.bilibili.com/video/BV1Je4y1A78j)
87. 宁波428日料自助餐，给后厨吃懵了，用海胆给我们上了一课！ [:link:](//www.bilibili.com/video/BV1ns4y1W72m)
88. 10道10元超简单素菜，我是不相信你会翻车的 [:link:](//www.bilibili.com/video/BV1YG4y1U7G4)
89. 南方女生第一次吃东北麻辣拌！ 13元一大袋！满满的麻酱太香了！ [:link:](//www.bilibili.com/video/BV1By4y1D7sR)
90. Cookies｜超高难度～国风剪纸兔切片饼干！详细教程！ [:link:](//www.bilibili.com/video/BV1je4y1N71S)
</details>
<details>
<summary>91 ~ 100</summary>

91. 英雄联盟马服是什么梗【梗指南】 [:link:](//www.bilibili.com/video/BV1Zs4y1W75m)
92. 高启强究竟有多伪善？李响安欣的“镜像共生”好在哪里？深度剖析《狂飙》的文学性！我们到底何而为人？ [:link:](//www.bilibili.com/video/BV18d4y1n748)
93. 离谱！在女友骂我时突然给她喂东西吃…她好像真被哄好了？ [:link:](//www.bilibili.com/video/BV1iM4y1X7GL)
94. 听说指导组下一站是芜湖 [:link:](//www.bilibili.com/video/BV1B84y1572t)
95. 扒了狂飙大嫂的健身计划，她是真懂训练！ [:link:](//www.bilibili.com/video/BV1cd4y1n7X2)
96. 这澡洗的 [:link:](//www.bilibili.com/video/BV1He4y1A7aF)
97. 100元在冰岛超市能买什么？鲸鱼肉！鲨鱼！海豹这里竟然都有卖！ [:link:](//www.bilibili.com/video/BV1md4y1n7VV)
98. 给大家介绍一下我的职业 [:link:](//www.bilibili.com/video/BV19v4y1t7RF)
99. 这电视上的节目，就是精彩啊 [:link:](//www.bilibili.com/video/BV1zx4y157QC)
100. hack的乐趣生活｜纸飞机的升级版！太炫了吧！！ [:link:](//www.bilibili.com/video/BV19y4y1X7wo)
</details></details>